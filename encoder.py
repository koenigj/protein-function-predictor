import numpy as np
import pandas as pd
from sklearn.preprocessing import OneHotEncoder

categories = ["A    R    N    D    C    Q    E    G    H    I    L    K    M    F    P    S    T    W    Y    V    B  "
              "  Z    X    *".split()]  # Just for visualization, not used
length = 400  # All proteins must be this long or shorter.


#  Generate sparse hot-encoding matrix for a given sequence
def one_hot_encoding(seq: str):
    # Pad sequence if it is too short
    seq = seq.ljust(length, 'Q')  # Since Q does not code for any AA, the resulting one-hot-encoding is [0, 0, ,,, 0]
    encoder = OneHotEncoder(categories=categories, handle_unknown='ignore')
    df = pd.DataFrame(list(seq))
    df.columns = ['AA', ]
    seq_1hot = encoder.fit_transform(df)
    return seq_1hot.toarray()
