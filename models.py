import sys
import sklearn
from sklearn.model_selection import StratifiedKFold
from sklearn.linear_model import SGDClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
import tensorflow as tf
from tensorflow import keras
import numpy as np
import os
from pathlib import Path


def train_cnn_rnn(X_train, y_train, length):
    # //TODO: HYperparameter Anzahl an Convolutional Kernels, Anzahl an filters
    model = keras.models.Sequential([
        keras.layers.Conv1D(filters=round(length / 2), kernel_size=4, strides=2, input_shape=[length, 24]),
        keras.layers.Bidirectional(
            keras.layers.GRU(round(length / 2))
        ),
        keras.layers.Dense(1)

    ])
    model.compile(loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                  optimizer=tf.keras.optimizers.Adam(learning_rate=0.00001),
                  metrics=['accuracy']
                  )
    # TODO Hyperparameters: Number of Hidden Layers (1), number of neurons in each layer
    # TODO architecture of LSTM neurons
    history = model.fit(X_train, y_train, epochs=4, validation_split=0.1)
    return model


def train_rnn(X_train, y_train, length):
    model = keras.models.Sequential([
        tf.keras.layers.Masking(mask_value=0, input_shape=[length, 24]),
        keras.layers.Bidirectional(
            keras.layers.LSTM(length)
        ),
        keras.layers.Dense(1)

    ])
    model.compile(loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                  optimizer=tf.keras.optimizers.Adam(learning_rate=0.00001),
                  metrics=['accuracy']
                  )
    # TODO Hyperparameters: Number of Hidden Layers (1), number of neurons in each layer
    # TODO architecture of LSTM neurons
    print(model.summary)
    history = model.fit(X_train, y_train, epochs=4, validation_split=0.1)
    return model


def train_sgd(X, y):
    sgd = SGDClassifier(random_state=42)
    nsamples, nx, ny = X.shape
    X = X.reshape((nsamples, nx * ny))
    y = (y == 1)
    sgd.fit(X, y)
    return sgd


def cross_validate_sgd(X, y, folds):
    skfold = StratifiedKFold(n_splits=folds, shuffle=True)
    nsamples, nx, ny = X.shape
    X = X.reshape((nsamples, nx * ny))
    y = (y == 1)
    results = []
    for train, test in skfold.split(X, y):
        sgd = SGDClassifier()
        X_train = X[train]
        y_train = y[train]
        X_test = X[test]
        y_test = y[test]

        sgd.fit(X_train, y_train)
        y_pred = sgd.predict(X_test)
        tp, tn, fp, fn = 1, 1, 1, 1
        for j in range(len(y_test)):
            if y_test[j]:
                if y_pred[j]:
                    tp += 1
                else:
                    fn += 1
            else:
                if y_pred[j]:
                    fp += 1
                else:
                    tn += 1
        acc = (tp + tn) / len(y_test)
        precision = tp / (tp + fp)
        recall = tp / (tp + fn)
        f1 = tp / (tp + ((fn + fp) / 2))
        results.append([acc, precision, recall, f1, tp, fp, tn, fn, 1])
    results = np.array(results)
    return results.mean(axis=0).tolist()


def train_gpc(X, y):
    nsamples, nx, ny = X.shape
    X = X.reshape((nsamples, nx * ny))
    y = (y == 1)
    gpc = GaussianProcessClassifier(random_state=0).fit(X, y)
    return gpc


def test_keras_model(X_test, y_test, model):
    y_pred = model(X_test)
    tp, tn, fp, fn = 1, 1, 1, 1  # Calculate True Positives, True negatives,...
    for j in range(len(y_test)):
        if y_test[j] == 1:
            if y_pred[j][0] > 0:
                tp += 1
            else:
                fn += 1
        else:
            if y_pred[j][0] > 0:
                fp += 1
            else:
                tn += 1
    acc = (tp + tn) / len(y_test)
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1 = tp / (tp + ((fn + fp) / 2))
    y_test_tensor = [y_test[i:i + 1] for i in range(0, len(y_test))]
    bce = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    bce = bce(y_test_tensor, y_pred).numpy()
    return acc, precision, recall, f1, tp, fp, tn, fn, bce


def calculate_stats(y_true, y_pred):
    if type(y_pred) == int or type(y_pred) == float:  # Bring all types of predictions into boolean format
        y_pred = (y_pred > 0.5)
    if type(y_true) == int or type(y_true) == float:
        y_true = (y_true > 0.5)

    tp, tn, fp, fn = 1, 1, 1, 1
    for j in range(len(y_true)):
        if y_true[j]:
            if y_pred[j]:
                tp += 1
            else:
                fn += 1
        else:
            if y_pred[j]:
                fp += 1
            else:
                tn += 1
    acc = (tp + tn) / len(y_true)
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1 = tp / (tp + ((fn + fp) / 2))
    return [acc, precision, recall, f1, tp, fp, tn, fn, 1]
