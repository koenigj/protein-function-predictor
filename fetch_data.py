import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
import tensorflow as tf
import argparse
import requests
import urllib3
import os
import encoder
import models

encode = encoder.one_hot_encoding


def download_fasta(URL: str):
    urllib3.disable_warnings()
    r = requests.get(URL, auth=('anonymous', 'Jakob.Koenig@campus.lmu.de'), verify=False)
    if r.status_code != 200:  # Website does not exist
        print("Error: GO-term not found")
        exit(0)
    fasta = r.text.split('\n')  # Split go file into list of lines
    r.close()
    return fasta


def read_fasta(fasta: list):
    data = []
    seq = []
    for line in fasta[1:]:  # Do not read first '>' of fasta file
        if line.startswith('>'):
            seq = "".join(seq)  # Use encoding on the read sequence
            if len(seq) <= encoder.length:
                data.append(encode(seq))
            seq = []
        else:
            seq.append(line)
    return data


#  Saves fraction of a fasta file inputted as list of lines
def save_fasta(fasta: list, file, start=0, limit=float('inf')):
    with open(file, 'w') as f:
        counter = 0
        for line in fasta:
            if line.startswith('>'):
                if counter < start:
                    counter += 1
                elif counter > limit:
                    break
                else:
                    f.write(line)
                    f.write('\n')
            else:
                if counter >= start:
                    f.write(line)
                    f.write('\n')


""" Generate data set """


def fetch(URL_positive: str, URL_negative: str, go_term: str):
    X = read_fasta(download_fasta(URL_positive))  # Download and store all proteins in the postive class
    print("Downloaded " + str(len(X)) + " proteins from:" + URL_positive)
    while len(X) == 0:
        print("Trying again for " + URL_positive)
        X = read_fasta(download_fasta(URL_positive))
        print("Downloaded " + str(len(X)) + " proteins from:" + URL_positive)
    y = [1 for i in range(len(X))]  # Values to be predicted for positive proteins
    Xn = read_fasta(download_fasta(URL_negative + '&limit=' + str(round(len(X) * 2))))  # Extend with the
    Xn = Xn[:len(X)]  # same number of negative proteins
    X.extend(Xn)
    size = len(X)
    print("Downloaded " + str(size - len(y)) + " random proteins not annotated with GO:" + go_term)
    y.extend([0 for i in range(size - len(y))])  # Fill the remaining labels with 0s
    X = np.array(X)  # Convert to numpy array
    y = np.array(y)
    print(X.shape)
    print("Converted to numpy array")
    size = len(X)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, stratify=y)
    return X_train, X_test, y_train, y_test
