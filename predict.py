#!/usr/bin/env python3

import numpy as np
import tensorflow as tf
import argparse
import encoder

import os

parser = argparse.ArgumentParser(description="Predict target proteins on a precomputed model. The predictions will be "
                                             "written to stdout")

parser.add_argument('--model', help='Directory of a precomputed model ready for prediction', required=True)
parser.add_argument('--fasta', help='Fasta file containing proteins to be predicted.')
parser.add_argument('--seq', help='Protein sequences to be predicted. The predictions will be in the same order.',
                    nargs='*')
"""             Usage
predict.py [-h] --model MODEL [--fasta FASTA] [--seq [SEQ ...]]

Predict target proteins on a precomputed model. The predictions will be
written to stdout

optional arguments:
  -h, --help       show this help message and exit
  --model MODEL    Directory of a precomputed model ready for prediction
  --fasta FASTA    Fasta file containing proteins to be predicted.
  --seq [SEQ ...]  Protein sequences to be predicted. The predictions will be
                   in the same order.

"""
args = parser.parse_args()

directory = args.model
fasta = args.fasta
seq = args.seq

model = tf.keras.models.load_model(directory)  # Load model from directory
length = model.layers[0].input_shape[0]  # Retrieve length from input shape of models first layer
if not length:
    length = int(open(directory + "/length").read())  # Retrieve length from additional file in model directory
print(length)
encoder.length = length
encode = np.vectorize(encoder.one_hot_encoding)

names = []
sequences = []  # Holds 1hot arrays of passed sequences
indices = []  # Holds indices of proteins that needed to be skipped

if seq and len(seq) > 0:
    for i in range(len(seq)):
        protein = seq[i].strip()
        if len(protein) <= length:
            sequences.append(encoder.one_hot_encoding(protein))
        else:
            indices.append(i)  # Protein too long
        names.append(str(i))


if fasta and os.path.isfile(fasta):
    with open(fasta, 'r') as file:
        fasta = [line.rstrip() for line in file]
    seq = []
    index = len(indices) + len(sequences)
    first = True
    for line in fasta:
        if first:
            names.append(line.split('|')[1])
            first = False
        elif line.startswith('>'):
            names.append(line.split('|')[1])
            seq = "".join(seq)
            if len(seq) <= encoder.length:
                sequences.append(encode(seq))
            else:
                indices.append(index)
            index += 1
            seq = []
        else:
            seq.append(line)
    seq = "".join(seq)
    if len(seq) <= encoder.length:
        sequences.append(encode(seq))
    else:
        indices.append(index)


y_pred = model(np.array(sequences))
npops = 0  # Number of skipped proteins
for i in range(len(sequences) + len(indices)):
    print(names[i], ":", sep='', end=' ')
    if len(indices) > 0 and i == indices[0]:
        print('NaN')
        indices.pop(0)
        npops += 1
    else:
        print(y_pred[i - npops][0].numpy())
