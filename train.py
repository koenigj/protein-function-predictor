#!/usr/bin/env python3

import numpy as np
from sklearn.utils import shuffle
import tensorflow as tf
import argparse
from sklearn.model_selection import train_test_split
import encoder
import fetch_data
import models
from pathlib import Path
import time
import os

""" Parse Arguments"""
parser = argparse.ArgumentParser(description="Train AI for a given GO-term or use a precomputed model, then predict "
                                             "target proteins. The predictions will be written to an output file.33")
parser.add_argument('--go', help='GO-term', required=True, type=str)
parser.add_argument('--limit', help='Limit the number of proteins used for training to increase training speed',
                    type=int, default=400000)
parser.add_argument('--maxlength', help='Maximum length of proteins eligible for prediction', type=int, default=400)
parser.add_argument('--dir', help='Directory to save model', required=True)
parser.add_argument('--test', help='Hold back  a portion of data to test model', action='store_true')

"""         Usage             
train.py [-h] --go GO [--limit LIMIT] [--maxlength MAXLENGTH] --dir DIR
                [--test]

Train AI for a given GO-term or use a precomputed model, then predict target
proteins. The predictions will be written to an output file.33

optional arguments:
  -h, --help            show this help message and exit
  --go GO               GO-term
  --limit LIMIT         Limit the number of proteins used for training to
                        increase training speed
  --maxlength MAXLENGTH
                        Maximum length of proteins eligible for prediction
  --dir DIR             Directory to save model
  --test                Hold back a portion of data to test model

"""

args = parser.parse_args()
go_term = args.go
limit = args.limit
maxlength = args.maxlength
directory = args.dir
test = args.test


encoder.length = maxlength + maxlength % 2  # Always keep length at round number

directory = Path(directory)
directory.mkdir(parents=True, exist_ok=True)  # Create directory, throws error if unable


if go_term.startswith("GO:"):
    go_term = go_term[3:]  # Cut the leading 'GO:'
URL_positive = 'https://www.uniprot.org/uniprot/?query=cluster:(uniprot:(go:' + go_term + \
               ')%20identity:0.9)&format=fasta&sort=annotation_score'


if limit:
    URL_positive = URL_positive + '&limit=' + str(limit)
if maxlength:
    encoder.length = maxlength

X_train, X_test, y_train, y_test = fetch_data.fetch(URL_positive, URL_negative, go_term)
size = len(X_train)


model = models.train_cnn_rnn(X_train, y_train, maxlength)
model.save(directory)
with open(directory / 'length', 'w') as file:
    file.write(str(maxlength))

if test:
    print("RNN using website: " + URL_positive)
    print('data set size:', size, "Max length:", maxlength)
    print("accuracy", "precision", "recall", "f1", "tp", "fp", "tn", "fn", "bce", sep='\t')
    print(*models.test_keras_model(X_test, y_test, model))